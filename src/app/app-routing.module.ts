import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoginComponent} from './projectPM/auth/login/login.component';
import {RegisterComponent} from './projectPM/auth/register/register.component';
import {LogoutComponent} from './projectPM/auth/logout/logout.component';
import {AppGuard} from './app.guard';
import {EmployeeComponent} from './projectPM/employee/employee.component';
import {DepartmentsComponent} from './projectPM/departments/departments.component';
import {ApplicantsComponent} from './projectPM/applicants/applicants.component';
import {SalaryComponent} from './projectPM/salary/salary.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    canActivate: [AppGuard],
    children: [
      {
        path: 'employees',
        component: EmployeeComponent
      },
      {
        path: 'applicants',
        component: ApplicantsComponent
      },
      {
        path: 'departments',
        component: DepartmentsComponent
      },
      {
        path: 'salary',
        component: SalaryComponent
      },
      {
        path: 'logout',
        component: LogoutComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
