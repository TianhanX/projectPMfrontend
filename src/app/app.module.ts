import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import { EmployeeComponent } from './projectPM/employee/employee.component';
import { SalaryComponent } from './projectPM/salary/salary.component';
import {LoginComponent} from './projectPM/auth/login/login.component';
import { LogoutComponent } from './projectPM/auth/logout/logout.component';
import { RegisterComponent } from './projectPM/auth/register/register.component';
import {FormsModule} from '@angular/forms';
import { AddEmployeesComponent } from './projectPM/admin/admin-employees/add-employees/add-employees.component';
import { UpdateEmployeesComponent } from './projectPM/employee/update-employees/update-employees.component';
import { DepartmentsComponent } from './projectPM/departments/departments.component';
import { ApplicantsComponent } from './projectPM/applicants/applicants.component';
import { AddApplicantsComponent } from './projectPM/applicants/add-applicants/add-applicants.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {AppGuard} from './app.guard';
import { AdminEmployeesComponent } from './projectPM/admin/admin-employees/admin-employees.component';
import { AdminApplicantsComponent } from './projectPM/admin/admin-applicants/admin-applicants.component';
import { AdminSalaryComponent } from './projectPM/admin/admin-salary/admin-salary.component';
import {CommonModule} from '@angular/common';
import { AddDepartmentsComponent } from './projectPM/departments/add-departments/add-departments.component';
import { UpdateDepartmentsComponent } from './projectPM/departments/update-departments/update-departments.component';
import { UpdateAdminEmployeesComponent } from './projectPM/admin/admin-employees/update-admin-employees/update-admin-employees.component';


@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    SalaryComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    AddEmployeesComponent,
    UpdateEmployeesComponent,
    DepartmentsComponent,
    ApplicantsComponent,
    AddApplicantsComponent,
    AdminEmployeesComponent,
    AdminApplicantsComponent,
    AdminSalaryComponent,
    AddDepartmentsComponent,
    UpdateDepartmentsComponent,
    UpdateAdminEmployeesComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [AppGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
