import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../../service/applicant.service';

@Component({
  selector: 'app-admin-applicants',
  templateUrl: './admin-applicants.component.html',
  styleUrls: ['./admin-applicants.component.css']
})
export class AdminApplicantsComponent implements OnInit {

  applicants: any;

  constructor(private applicantService: ApplicantService) { }

  ngOnInit(): void {
    this.applicantService.GetApplicants().subscribe(res => {
      this.applicants = res;
    });
  }

  deleteApplicant(applicant): void{
    this.applicantService.DeleteApplicants(applicant).subscribe(res => {
      console.log(res);
    });
  }
}
