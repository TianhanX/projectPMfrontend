import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../../service/employee.service';

@Component({
  selector: 'app-add-employees',
  templateUrl: './add-employees.component.html',
  styleUrls: ['./add-employees.component.css']
})
export class AddEmployeesComponent implements OnInit {

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
  }

  onSubmit(employees): void{
    this.employeeService.AddEmployees(employees).subscribe(res => {
      this.employeeService = res;
    });
  }
}
