import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../service/employee.service';

@Component({
  selector: 'app-admin-employees',
  templateUrl: './admin-employees.component.html',
  styleUrls: ['./admin-employees.component.css']
})
export class AdminEmployeesComponent implements OnInit {
  employees: any;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    //判断是否是admin
    this.employeeService.GetEmployees().subscribe(res => {
      this.employees = res;
    });
  }

  deleteEmployee(employee): void{
    this.employeeService.DeleteEmployees(employee).subscribe(res => {
      console.log(res);
    });
  }
}
