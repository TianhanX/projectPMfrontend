import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminEmployeesComponent } from './update-admin-employees.component';

describe('UpdateAdminEmployeesComponent', () => {
  let component: UpdateAdminEmployeesComponent;
  let fixture: ComponentFixture<UpdateAdminEmployeesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateAdminEmployeesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
