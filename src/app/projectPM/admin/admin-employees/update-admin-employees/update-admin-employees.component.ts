import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../../service/employee.service';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-update-admin-employees',
  templateUrl: './update-admin-employees.component.html',
  styleUrls: ['./update-admin-employees.component.css']
})
export class UpdateAdminEmployeesComponent implements OnInit {
  employee: any;
  employees: any;
  id: any;

  constructor(private employeeService: EmployeeService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
    this.employeeService.GetEmployeesById(this.id).subscribe(res => this.employeeService.SetData(res));
  }

  save(employees): void {
    this.employeeService.UpdateEmployees(employees).subscribe(res => {
      this.employees = res;
    });
  }
}
