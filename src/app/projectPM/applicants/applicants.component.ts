import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../service/applicant.service';

@Component({
  selector: 'app-applicants',
  templateUrl: './applicants.component.html',
  styleUrls: ['./applicants.component.css']
})
export class ApplicantsComponent implements OnInit {
  applicants: any;

  constructor(private applicantService: ApplicantService) { }

  ngOnInit(): void {
    this.applicantService.GetApplicants().subscribe(res => {
      this.applicants = res;
    });
  }
}
