import { Component, OnInit } from '@angular/core';
import {DepartmentService} from '../../service/department.service';

@Component({
  selector: 'app-add-departments',
  templateUrl: './add-departments.component.html',
  styleUrls: ['./add-departments.component.css']
})
export class AddDepartmentsComponent implements OnInit {

  constructor(private departmentService: DepartmentService) { }

  ngOnInit(): void {
  }

  onSubmit(departments): void{
    this.departmentService.AddDepartments(departments).subscribe(res => {
      this.departmentService = res;
    });
  }
}
