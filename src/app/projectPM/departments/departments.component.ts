import { Component, OnInit } from '@angular/core';
import {DepartmentService} from '../service/department.service';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  departments: any
  constructor(private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.departmentService.GetDepartments().subscribe(res => {
      this.departments = res;
    });
  }

  deleteDepartment(department): void{
    this.departmentService.DeleteDepartments(department).subscribe(res => {
      console.log(res);
    });
  }

}
