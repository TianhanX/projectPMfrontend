import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../service/employee.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {DepartmentService} from '../../service/department.service';

@Component({
  selector: 'app-update-departments',
  templateUrl: './update-departments.component.html',
  styleUrls: ['./update-departments.component.css']
})
export class UpdateDepartmentsComponent implements OnInit {
  departments: any
  id: any

  constructor(private departmentService: DepartmentService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
    this.employeeService.GetEmployeesById(this.id).subscribe(res => this.departmentService);
  }

  save(employees): void {
    this.employeeService.UpdateEmployees(employees).subscribe(res => {
      this.employees = res;
    });
  }
}
