import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../service/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employees: any;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    //判断是否是admin
    this.employeeService.GetEmployees().subscribe(res => {
      this.employees = res;
    });
  }

  deleteEmployee(employee): void{
    this.employeeService.DeleteEmployees(employee).subscribe(res => {
      console.log(res);
    });
  }
}
