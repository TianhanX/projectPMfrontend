import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {EmployeeService} from '../../service/employee.service';

@Component({
  selector: 'app-update-employees',
  templateUrl: './update-employees.component.html',
  styleUrls: ['./update-employees.component.css']
})
export class UpdateEmployeesComponent implements OnInit {
  employee: any;
  employees: any;
  id: any;

  constructor(private employeeService: EmployeeService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
    this.employeeService.GetEmployeesById(this.id).subscribe(res => this.employeeService.SetData(res));
  }

  save(employees): void {
    this.employeeService.UpdateEmployees(employees).subscribe(res => {
      this.employees = res;
    });
  }
}
