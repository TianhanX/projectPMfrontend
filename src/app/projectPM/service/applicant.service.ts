import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApplicantService {

  private API_URL = `${environment.API_URL}`;

  constructor(private http: HttpClient, private router: Router) { }

  GetApplicants(): Observable<any> {
    return this.http.get(this.API_URL + '/applicants', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  DeleteApplicants(applicant): Observable<any> {
    const id = applicant.id;
    return this.http.delete(this.API_URL + '/applicants/' + id, {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }
}
