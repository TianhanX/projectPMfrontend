import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  private API_URL = `${environment.API_URL}`;

  constructor(private http: HttpClient, private router: Router) { }

  GetDepartments(): Observable<any> {
    return this.http.get(this.API_URL + '/departments', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  GetDepartmentsById(id: any): Observable<any> {
    return this.http.get(this.API_URL + '/departments', id)
      .pipe(tap((res) => {
        return res;
      }));
  }

  AddDepartments(departments): Observable<any> {
    return this.http.post(this.API_URL + '/departments', departments)
      .pipe(tap((res) => {
        console.log(res)
        if (res.success) { }
      }));
  }

  UpdateDepartments(departments): Observable<any> {
    return this.http.put(this.API_URL + '/departments', departments)
      .pipe(tap((res) => {
        console.log(res)
        if (res.success) {
          this.router.navigate(['/departments']);
        }
      }));
  }

  DeleteDepartments(department): Observable<any> {
    const id = department.id;
    return this.http.delete(this.API_URL + '/departments/' + id, {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }
}
