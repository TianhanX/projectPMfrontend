import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private API_URL = `${environment.API_URL}`;

  constructor(private http: HttpClient, private router: Router) { }

  GetEmployees(): Observable<any> {
    return this.http.get(this.API_URL + '/employees', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  GetEmployeesById(id: any): Observable<any> {
    return this.http.get(this.API_URL + '/employees', id)
      .pipe(tap((res) => {
        return res;
      }));
  }

  AddEmployees(employees): Observable<any> {
    return this.http.post(this.API_URL + '/employees', employees)
      .pipe(tap((res) => {
        console.log(res)
        if (res.success) { }
      }));
  }

  UpdateEmployees(employees): Observable<any> {
    return this.http.put(this.API_URL + '/employees', employees)
      .pipe(tap((res) => {
        console.log(res)
        if (res.success) {
          this.router.navigate(['/employees']);
        }
      }));
  }

  DeleteEmployees(employee): Observable<any> {
    const id = employee.id;
    return this.http.delete(this.API_URL + '/employees/' + id, {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  SetData(res: any) {
    
  }
}
