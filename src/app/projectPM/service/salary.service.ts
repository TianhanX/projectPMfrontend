import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  private API_URL = `${environment.API_URL}`;

  constructor(private http: HttpClient, private router: Router) { }

  GetSalary(): Observable<any> {
    return this.http.get(this.API_URL + '/salary', {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }

  GetSalaryById(id: any): Observable<any> {
    return this.http.get(this.API_URL + '/salary', id)
      .pipe(tap((res) => {
        return res;
      }));
  }

  AddSalary(salary): Observable<any> {
    return this.http.post(this.API_URL + '/salary', salary)
      .pipe(tap((res) => {
        console.log(res)
        if (res.success) { }
      }));
  }

  UpdateSalary(salary): Observable<any> {
    return this.http.put(this.API_URL + '/salary', salary)
      .pipe(tap((res) => {
        console.log(res)
        if (res.success) {
          this.router.navigate(['/salary']);
        }
      }));
  }

  DeleteSalary(salary): Observable<any> {
    const id = salary.id;
    return this.http.delete(this.API_URL + '/salary/' + id, {withCredentials: true})
      .pipe(tap((res) => {
        return res;
      }));
  }
}
